# vK8s demo
- [ ] stages-demo ns
- [ ] new vsite, selector demo == stages
- [ ] add tags to udf
- [ ] create vk8s
- [ ] deploy hipstershop
- [ ] create ves objects
- [ ] show 
- [ ] new aws credentials
- [ ] new aws vpc site
- [ ] add tags to aws

# mk8s demo
- [ ] ves objects prepared
- [ ] deploy argo to udf
- [ ] deploy garafana, kad via argo
- [ ] test grafana `rate(http_requests_total{endpoint="/"}[5m])`
