# WAAP Demo
## WAAP In RE
- [ ] origin `arcadia.emea.f5se.com`
   - [ ] no check ssl
- [ ] LB `arcadia-stages.emea-ent.f5demos.com`
### WAAP Policy
- [ ] Bot protection - Suspicious - Block
- [ ] Blocking page
 `string:///PHN0eWxlPmJvZHkgeyBmb250LWZhbWlseTogU291cmNlIFNhbnMgUHJvLCBzYW5zLXNlcmlmOyB9PC9zdHlsZT4KPGh0bWwgc3R5bGU9Im1hcmdpbjogMDsiPjxoZWFkPjx0aXRsZT5SZWplY3RlZCBSZXF1ZXN0PC90aXRsZT48L2hlYWQ+Cjxib2R5IHN0eWxlPSJtYXJnaW4gOiAwOyI+CjxkaXYgc3R5bGU9ImJhY2tncm91bmQtY29sb3I6ICMwNDZiOTk7IGhlaWdodDogNDBweDsgd2lkdGg6IDEwMCU7Ij48L2Rpdj4KPGRpdiBzdHlsZT0ibWluLWhlaWdodDogMTAwcHg7IGJhY2tncm91bmQtY29sb3I6IHdoaXRlOyB0ZXh0LWFsaWduOiBjZW50ZXI7Ij48L2Rpdj4KPGRpdiBzdHlsZT0iYmFja2dyb3VuZC1jb2xvcjogI2ZkYjgxZTsgaGVpZ2h0OiA1cHg7IHdpZHRoOiAxMDAlOyI+PC9kaXY+CjxkaXYgaWQ9Im1haW4tY29udGVudCIgc3R5bGU9IndpZHRoOiAxMDAlOyAiPgogIDx0YWJsZSB3aWR0aD0iMTAwJSI+CiAgICA8dHI+PHRkIHN0eWxlPSJ0ZXh0LWFsaWduOiBjZW50ZXI7Ij4KCSAgPGRpdiBzdHlsZT0ibWFyZ2luLWxlZnQ6IDUwcHg7Ij4KICAgICAgICA8ZGl2IHN0eWxlPSJtYXJnaW4tYm90dG9tOiAzNXB4OyI+PGJyLz4KICAgICAgICAgIDxzcGFuIHN0eWxlPSJmb250LXNpemU6IDQwcHQ7IGNvbG9yOiAjMDQ2Yjk5OyI+UmVqZWN0ZWQgUmVxdWVzdDwvc3Bhbj4KICAgICAgICA8L2Rpdj4KICAgICAgICA8ZGl2IHN0eWxlPSJmb250LXNpemU6IDE0cHQ7Ij4KICAgICAgICAgIDxwPlRoZSByZXF1ZXN0ZWQgVVJMIHdhcyByZWplY3RlZC4gUGxlYXNlIGNvbnN1bHQgd2l0aCB5b3VyIGFkbWluaXN0cmF0b3IuPC9wPgogICAgICAgICAgPHA+WW91ciBTdXBwb3J0IElEIGlzOiA8c3BhbiBzdHlsZT0iY29sb3I6cmVkOyBmb250LXdlaWdodDpib2xkIj57e3JlcXVlc3RfaWR9fTwvc3Bhbj48L3A+CgkJICA8cD48YSBocmVmPSJqYXZhc2NyaXB0Omhpc3RvcnkuYmFjaygpIj5bR28gQmFja108L2E+PC9wPgogICAgICAgICAgPHA+PGltZyBzcmM9Imh0dHBzOi8vaW1hZ2Uuc3ByZWFkc2hpcnRtZWRpYS5uZXQvaW1hZ2Utc2VydmVyL3YxL21wL3Byb2R1Y3RzL1QxNDU5QTgzOU1QQTQ0NTlQVDI4RDE4OTUwMTU4MEZTMTU2NC92aWV3cy8xLHdpZHRoPTU1MCxoZWlnaHQ9NTUwLGFwcGVhcmFuY2VJZD04MzksYmFja2dyb3VuZENvbG9yPUYyRjJGMi9jYXQtcGV3LXBldy1tYWRhZmFrYXMtYXV0b2NvbGxhbnQuanBnIj48L3A+CiAgICAgICAgPC9kaXY+CiAgICAgIDwvZGl2PgogICAgPC90ZD48L3RyPgogIDwvdGFibGU+CjwvZGl2Pgo8ZGl2IHN0eWxlPSJiYWNrZ3JvdW5kLWNvbG9yOiAjMjIyMjIyOyBwb3NpdGlvbjogZml4ZWQ7IGJvdHRvbTogMHB4OyBoZWlnaHQ6IDQwcHg7IHdpZHRoOiAxMDAlOyB0ZXh0LWFsaWduOiBjZW50ZXI7Ij48L2Rpdj4KPC9ib2R5Pgo8L2h0bWw+`
- [ ] tests:\
`curl -k https://arcadia-jdo.emea-ent.f5demos.com/`\
``curl 'https://arcadia-jdo.emea-ent.f5demos.com/trading/rest/buy_stocks.php' 
-H 'authorization: Basic YWRtaW46aWxvdmVibHVl' 
-H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36' 
-H 'content-type: application/json; charset=UTF-8' 
-H 'x-requested-with: XMLHttpRequest' 
--data-raw '{"trans_value":330,"qty":2,"company":"FFIV","action":"buy","stock_price":165}' 
--compressed``
- [ ] projit logy
- [ ] XSS do loginu \
`' or 1=1#` \
`SELECT UserId, Name, Password FROM Users WHERE UserId = 105 or 1=1;`
- logy, exclusion rule
### Positive security
- [ ] new service policy
- [ ] custom rule list, new rule IP Reputation
- [ ] Action Dedny, List of IP Threat Campaigns
- [ ] new rule, file-type-deny
- [ ] HTTP Path, regex `(.doc|.docx|.pdf|.exe|.bat)$`
### Shape
- [ ] HTTP LB - new bot policy
- [ ] buy-stock
- [ ] HTTP Method - POST
- [ ] Path: `/trading/rest/buy_stocks.php`
- [ ] Test curlem
- [ ] Logy, analytika

## Origin in CE

## WAAP in CE

